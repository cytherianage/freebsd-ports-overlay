--- twofish.c.orig	2022-11-26 20:28:07 UTC
+++ twofish.c
@@ -42,5 +42,5 @@ DL_EXPORT(void) exp_Twofish_decrypt(Twofish_key * xkey
 We need a stub init_twofish function so the module will link as a proper module.
 Do not import _twofish from python; it will not work since _twofish is not a *real* module
 */
-PyMODINIT_FUNC init_twofish(void) { }
-PyMODINIT_FUNC PyInit__twofish(void) { }
+PyMODINIT_FUNC init_twofish(void) { return NULL; }
+PyMODINIT_FUNC PyInit__twofish(void) { return NULL; }
